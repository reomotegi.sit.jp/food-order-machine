import javax.swing.*;
import javax.swing.text.AbstractDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class Assignment {
    private JPanel root;
    private JLabel topLabel;
    private JButton mainA;
    private JButton mainB;
    private JButton mainC;
    private JButton mainD;
    private JButton mainE;
    private JButton mainF;
    private JTextPane orderedItemList;
    private JLabel ordered;
    private JTextPane total;
    private JButton checkOut;
    private JTabbedPane tabbedPane1;
    private JButton sideA;
    private JButton sideB;
    private JButton sideC;
    private JButton sideF;
    private JButton sideE;
    private JButton sideD;
    private JLabel topLabel2;
    public int totalPrice =0;
    public String[][] mainMenu={{"Hamburg", "850"}, {"Steak", "1200"}, {"Beef Stew", "900"}, {"Fried Shrimp", "800"}, {"Omelet Rice", "750"}, {"Gratin", "700"}};
    public String[][] sideMenu ={{"Cake", "400"}, {"Pudding", "350"}, {"Ice Cream", "300"}, {"Coffee", "250"}, {"Tea", "220"}, {"Orange Juice", "200"}};

    void order(String food, int price) {
            SpinnerNumberModel model = new SpinnerNumberModel(0, 0, 10, 1);
            JSpinner spinner = new JSpinner(model);
            String[] select ={"Yes", "NO"};
            int confirmation=JOptionPane.showOptionDialog(null,
                    spinner,
                    "Please input the number of " + food + ".",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    select,
                    select[0]);
            if(confirmation==0) {
                Integer value = (Integer) model.getValue();
                if (value == 0) {
                    JOptionPane.showConfirmDialog(null, "Please select the quantity.",
                            "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Thank you for ordering " + food + " ! It will b served as soon as possible.",
                            "Thank you so much!",
                            JOptionPane.DEFAULT_OPTION);
                    String currentText = orderedItemList.getText();
                    orderedItemList.setText(currentText + food + " × " + value + " : " + String.format("%,d", price * value)  +" yen\n");
                    totalPrice += (price * value);
                    total.setText("Total : " + String.format("%,d", totalPrice) + " yen");
                }
            }
        }

    void showPrice(String[][] main, String[][] side, double tax){
        mainA.setText(main[0][0] + " : " + Math.round(Integer.parseInt(main[0][1])*(1+tax)) + " yen");
        mainB.setText(main[1][0] + " : " + Math.round(Integer.parseInt(main[1][1])*(1+tax)) + " yen");
        mainC.setText(main[2][0] + " : " + Math.round(Integer.parseInt(main[2][1])*(1+tax)) + " yen");
        mainD.setText(main[3][0] + " : " + Math.round(Integer.parseInt(main[3][1])*(1+tax)) + " yen");
        mainE.setText(main[4][0] + " : " + Math.round(Integer.parseInt(main[4][1])*(1+tax)) + " yen");
        mainF.setText(main[5][0] + " : " + Math.round(Integer.parseInt(main[5][1])*(1+tax)) + " yen");
        sideA.setText(side[0][0] + " : " + Math.round(Integer.parseInt(side[0][1])*(1+tax)) + " yen");
        sideB.setText(side[1][0] + " : " + Math.round(Integer.parseInt(side[1][1])*(1+tax)) + " yen");
        sideC.setText(side[2][0] + " : " + Math.round(Integer.parseInt(side[2][1])*(1+tax)) + " yen");
        sideD.setText(side[3][0] + " : " + Math.round(Integer.parseInt(side[3][1])*(1+tax)) + " yen");
        sideE.setText(side[4][0] + " : " + Math.round(Integer.parseInt(side[4][1])*(1+tax)) + " yen");
        sideF.setText(side[5][0] + " : " + Math.round(Integer.parseInt(side[5][1])*(1+tax)) + " yen");
    }

    void selection(){
        String[] selectValues={"For Here", "To go"};
        while(true) {
            int select = JOptionPane.showOptionDialog(null,
                    "Is this \"For here\" or \"To go\"?",
                    "Question",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    selectValues,
                    selectValues[0]
            );
            if (select == 0) {
                JOptionPane.showMessageDialog(null,
                        "Sales tax is 10%. All listed prices are including tax.",
                        "Sales Tax",
                        JOptionPane.INFORMATION_MESSAGE);
                showPrice(mainMenu, sideMenu,0.1);
                break;
            } else if (select == 1) {
                JOptionPane.showMessageDialog(null,
                        "Sales tax is 8%. All listed prices are including tax.",
                        "Sales Tax",
                        JOptionPane.INFORMATION_MESSAGE);
                showPrice(mainMenu, sideMenu,0.08);
                break;
            }
            else {
                JOptionPane.showConfirmDialog(null,
                        "Please select \"For here\" or \"To go\".",
                        "Error",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    void checkOut(){
        String[] select = {"Yes", "No"};
        int confirmation=JOptionPane.showOptionDialog(null,
                "Would you like to check out?",
                "Payment",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                select,
                select[0]);
        if(confirmation==0) {
            if (totalPrice == 0) {
                int option = JOptionPane.showConfirmDialog(null, "No dish selected",
                        "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(null,
                        "Thank you. The total price is " + String.format("%,d", totalPrice) + " yen.",
                        "We look forward to seeing you again!",
                        JOptionPane.DEFAULT_OPTION);
                orderedItemList.setText("");
                totalPrice =0;
                total.setText("Total : "+ totalPrice + " yen");
                mainA.setText(mainMenu[0][0]);
                mainB.setText(mainMenu[1][0]);
                mainC.setText(mainMenu[2][0]);
                mainD.setText(mainMenu[3][0]);
                mainE.setText(mainMenu[4][0]);
                mainF.setText(mainMenu[5][0]);
                sideA.setText(sideMenu[0][0]);
                sideB.setText(sideMenu[1][0]);
                sideC.setText(sideMenu[2][0]);
                sideD.setText(sideMenu[3][0]);
                sideE.setText(sideMenu[4][0]);
                sideF.setText(sideMenu[5][0]);
                try{
                    Thread.sleep(1000);
                }
                catch (InterruptedException e){
                }
                tabbedPane1.setSelectedIndex(0);
                selection();
            }
        }
    }
public Assignment() {
    mainA.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(mainA.getText().substring(mainA.getText().indexOf(": ")+2, mainA.getText().indexOf(" yen")));
            order(mainMenu[0][0], price);
        }
    });

    mainA.setIcon(new ImageIcon(this.getClass().getResource("Hamburg.jpg")
    ));

    mainB.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(mainB.getText().substring(mainB.getText().indexOf(": ")+2, mainB.getText().indexOf(" yen")));
            order(mainMenu[1][0], price);
        }
    });

    mainB.setIcon(new ImageIcon(this.getClass().getResource("Steak.jpg")
    ));

    mainC.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(mainC.getText().substring(mainC.getText().indexOf(": ")+2, mainC.getText().indexOf(" yen")));
            order(mainMenu[2][0],price);
        }
    });

    mainC.setIcon(new ImageIcon(this.getClass().getResource("Beef_Stew.jpg")
    ));

    mainD.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(mainD.getText().substring(mainD.getText().indexOf(": ")+2, mainD.getText().indexOf(" yen")));
            order(mainMenu[3][0], price);
        }
    });

    mainD.setIcon(new ImageIcon(this.getClass().getResource("Fried_Shrimp.jpg")
    ));

    mainE.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(mainE.getText().substring(mainE.getText().indexOf(": ")+2, mainE.getText().indexOf(" yen")));
            order(mainMenu[4][0], price);
        }
    });

    mainE.setIcon(new ImageIcon(this.getClass().getResource("Omelet_rice.jpg")
    ));

    mainF.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(mainF.getText().substring(mainF.getText().indexOf(": ")+2, mainF.getText().indexOf(" yen")));
            order(mainMenu[5][0], price);
        }
    });

    mainF.setIcon(new ImageIcon(this.getClass().getResource("Gratin.jpg")
    ));

    checkOut.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            checkOut();
        }
    });

    topLabel.addComponentListener(new ComponentAdapter() {
        @Override
        public void componentResized(ComponentEvent e) {
            mainA.setText(mainMenu[0][0]);
            mainB.setText(mainMenu[1][0]);
            mainC.setText(mainMenu[2][0]);
            mainD.setText(mainMenu[3][0]);
            mainE.setText(mainMenu[4][0]);
            mainF.setText(mainMenu[5][0]);
            sideA.setText(sideMenu[0][0]);
            sideB.setText(sideMenu[1][0]);
            sideC.setText(sideMenu[2][0]);
            sideD.setText(sideMenu[3][0]);
            sideE.setText(sideMenu[4][0]);
            sideF.setText(sideMenu[5][0]);
            selection();
        }
    });

    sideA.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(sideA.getText().substring(sideA.getText().indexOf(": ")+2, sideA.getText().indexOf(" yen")));
            order(sideMenu[0][0], price);
        }
    });

    sideA.setIcon(new ImageIcon(this.getClass().getResource("Cake.png")
    ));

    sideB.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(sideB.getText().substring(sideB.getText().indexOf(": ")+2, sideB.getText().indexOf(" yen")));
            order(sideMenu[1][0], price);
        }
    });

    sideB.setIcon(new ImageIcon(this.getClass().getResource("Pudding.jpg")
    ));

    sideC.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(sideC.getText().substring(sideC.getText().indexOf(": ")+2, sideC.getText().indexOf(" yen")));
            order(sideMenu[2][0], price);
        }
    });

    sideC.setIcon(new ImageIcon(this.getClass().getResource("Ice_Cream.jpg")
    ));

    sideD.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(sideD.getText().substring(sideD.getText().indexOf(": ")+2, sideD.getText().indexOf(" yen")));
            order(sideMenu[4][0], price);
        }
    });

    sideD.setIcon(new ImageIcon(this.getClass().getResource("Coffee.jpg")
    ));

    sideE.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(sideE.getText().substring(sideE.getText().indexOf(": ")+2, sideE.getText().indexOf(" yen")));
            order(sideMenu[4][0], price);
        }
    });

    sideE.setIcon(new ImageIcon(this.getClass().getResource("Tea.jpg")
    ));

    sideF.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int price=Integer.parseInt(sideF.getText().substring(sideF.getText().indexOf(": ")+2, sideF.getText().indexOf(" yen")));
            order(sideMenu[5][0], price);
        }
    });

    sideF.setIcon(new ImageIcon(this.getClass().getResource("Orange_Juice.jpg")
    ));
}

    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment");
        frame.setContentPane(new Assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(900,600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}